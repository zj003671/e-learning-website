import { Routes, Route } from "react-router-dom";
import "./App.css";
import Home from "./routes/Home";
import About from "./routes/About";
import Service from "./routes/Service";
import Contact from "./routes/Contact";
import Forum from "./routes/Forum";
import Login from "./routes/Login";
import SignUp from "./routes/SignUp";
import ScrollToTop from "./components/ScrollToTop";
import { UserContextProvider } from "./components/UserContext";
import Create from "./routes/Create";
import Post from "./routes/Post";
import Edit from "./routes/Edit";
import BubbleSort from "./components/topics/BubbleSort";
import SelectionSort from "./components/topics/SelectionSort";
import MergeSort from "./components/topics/MergeSort";
import Chatbot from "./routes/Chatbot";

function App() {
	return (
		<div className="App">
			<ScrollToTop />
			<UserContextProvider>
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="/about" element={<About />} />
					<Route path="/forum" element={<Forum />} />
					<Route path="/service" element={<Service />} />
					<Route path="/contact" element={<Contact />} />
					<Route path="/login" element={<Login />} />
					<Route path="/signup" element={<SignUp />} />
					<Route path="/create" element={<Create />} />
					<Route path="/post/:id" element={<Post />} />
					<Route path="/edit/:id" element={<Edit />} />
					<Route path="/chatbot" element={<Chatbot />} />

					<Route path="/bubble-sort" element={<BubbleSort />} />
					<Route path="/selection-sort" element={<SelectionSort />} />
					<Route path="/merge-sort" element={<MergeSort />} />
				</Routes>
			</UserContextProvider>
		</div>
	);
}

export default App;
