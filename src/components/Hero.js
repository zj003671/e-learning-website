import React from "react";
import "./HeroStyles.css";

const Hero = ({
	cName,
	heroImg,
	title,
	text,
	url,
	buttonClass,
	buttonText,
}) => {
	return (
		<>
			<div className={cName}>
				<img alt="HeroImg" src={heroImg}></img>
				<div className="hero-text">
					<h1>{title}</h1>
					<p>{text}</p>
					<a href={url} className={buttonClass}>
						{buttonText}
					</a>
				</div>
			</div>
		</>
	);
};

export default Hero;
