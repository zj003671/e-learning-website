import React from "react";
import { useState } from "react";

import "./FormStyles.css";

const SignupForm = () => {
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");

	async function signup(ev) {
		console.log("SignUp");
		ev.preventDefault();

		const response = await fetch("http://localhost:4000/signup", {
			method: "POST",
			body: JSON.stringify({ username, password }),
			headers: { "Content-Type": "application/json" },
		});

		if (response.status === 200) {
			alert("Signup Successful");
		} else {
			alert("Signup Failed!");
		}
	}

	return (
		<>
			<form className="signup" onSubmit={signup}>
				<h1>Sign Up</h1>
				<input
					type="text"
					placeholder="username"
					value={username}
					onChange={(ev) => setUsername(ev.target.value)}
				></input>
				<input
					type="password"
					placeholder="password"
					value={password}
					onChange={(ev) => setPassword(ev.target.value)}
				></input>
				<button>Sign Up</button>
			</form>
		</>
	);
};

export default SignupForm;
