export const CodeQuestions = {
	bubbblesort_coding_questions: [
		{
			id: "1",
			title: "Implementing Bubble Sort",
			info: `Bubble sort is a simple sorting algorithm that repeatedly iterates through an array, comparing adjacent elements and swapping them if they are in the wrong order. The largest element "bubbles" to the end of the array with each iteration.`,
			task: `Implement the bubble sort algorithm in a function that takes an array of numbers as input and returns the array in ascending order. Use clear variable names, comments, and indentation to make your code readable.`,
			code: `Input: [3, 1, 4, 2, 5]\nOutput: [1, 2, 3, 4, 5]`,
			hint: `- Use nested for loops to iterate through the array and compare elements.\n- Remember to swap elements using a temporary variable or destructuring assignment.\n- Keep track of whether any swaps occurred in each iteration to optimize for sorted arrays.`,
		},
		{
			id: "2",
			title: "Counting Swaps and Comparisons",
			info: `While bubble sort is simple, its performance is inefficient for large datasets. Understanding the number of comparisons and swaps performed can help assess its complexity.`,
			task: `Modify your bubble sort implementation to track the number of comparisons and swaps made during the sorting process. Return these counts along with the sorted array.`,
			code: `Input: [7, 2, 4, 1, 5, 3, 6]\nOutput: [1, 2, 3, 4, 5, 6, 7] (sorted array), 21 comparisons, 10 swaps`,
			hint: `- Increment counters for each comparison and swap operation within your nested loops.\n- Return the counts as part of the function's return value.`,
		},
		{
			id: "3",
			title: "Optimized Bubble Sort",
			info: `As bubble sort repeatedly iterates through the entire array even when it's mostly sorted, optimizations can be made to improve its performance.`,
			task: `Enhance your bubble sort implementation by adding an optimization that stops iterating once no swaps occur in an inner loop iteration. This indicates that the array is already sorted from that point onward.`,
			code: `Input: [5, 1, 4, 2, 8]\nOutput: [1, 2, 4, 5, 8] (sorted array) (lower number of iterations compared to the original bubble sort)`,
			hint: `- Use a swapped flag within the inner loop.<br />- Set swapped to true if a swap occurs, otherwise set it to false.\n- If swapped remains false after an inner loop iteration, it means no swaps were needed, indicating a sorted sub-array.\n- Break out of the outer loop in this case to avoid unnecessary iterations.`,
		},
	],
	selectionsort_coding_questions: [
		{
			question:
				"In the worst-case scenario, the number of comparisons performed by bubble sort is directly proportional to the _____________ of the input array. What is the missing word?",
			type: "FIB",
			correctAnswer: "(n^2)",
		},
	],
};
