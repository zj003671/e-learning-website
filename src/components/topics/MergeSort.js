import React from "react";

import { Navbar } from "../Navbar";
import Hero from "../Hero";
import Footer from "../Footer";

const MergeSort = () => {
	return (
		<>
			<Navbar />
			<Hero
				cName="hero-mid"
				heroImg="https://images.unsplash.com/photo-1637775297509-19767f6fc225?q=80&w=1254&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
				title="Merge Sort"
				buttonClass="hide"
			/>
			<h1>Merge Sort</h1>
			<Footer />
		</>
	);
};

export default MergeSort;
