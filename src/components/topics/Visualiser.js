/* eslint-disable no-unused-vars */
import React, { useState, useRef, useEffect } from "react";

import "./VisualiserStyles.css";
import { getQuickSortAnimations } from "./algorithms/QuickSort";
import { getBubbleSortAnimations } from "./algorithms/BubbleSort";
import { getSelectionSortAnimations } from "./algorithms/SelectionSort";
import { getInsertionSortAnimations } from "./algorithms/InsertionSort";
import { getHeapSortAnimations } from "./algorithms/HeapSort";

//Default Values
const DEFAULT_ARRAY_SIZE = 50;
const DEFAULT_ANIMATION_SPEED = 50;
const ARRAY_MIN_VALUE = 20;
const ARRAY_MAX_VALUE = 500;

const Visualiser = ({ sorting_method, sorting_text }) => {
	const [arraySize, setArraySize] = useState(DEFAULT_ARRAY_SIZE);
	const [animationSpeed, setAnimationSpeed] = useState(DEFAULT_ANIMATION_SPEED);
	const [array, setArray] = useState([]);
	const [disableButtons, setDisableButtons] = useState(false);
	const ref = useRef(null);

	const duplicateArray = array.slice();

	useEffect(() => {
		const newArray = [];
		for (let i = 0; i < arraySize; i++) {
			newArray.push(randomIntFromInterval(ARRAY_MIN_VALUE, ARRAY_MAX_VALUE));
		}
		const arrayBars = document.getElementsByClassName("arrayBar");
		for (let i = 0; i < arrayBars.length; i++) {
			arrayBars[i].style.backgroundColor = "gray";
		}
		setArray(newArray);
	}, [arraySize]);

	const resetArray = () => {
		const newArray = [];
		const arrayBars = document.getElementsByClassName("arrayBar");
		for (let i = 0; i < arrayBars.length; i++) {
			arrayBars[i].style.backgroundColor = "gray";
		}
		for (let i = 0; i < arraySize; i++) {
			newArray.push(randomIntFromInterval(ARRAY_MIN_VALUE, ARRAY_MAX_VALUE));
		}
		setArray(newArray);
	};

	//Function to give random values between a specified range
	const randomIntFromInterval = (min, max) => {
		return Math.floor(Math.random() * (max - min + 1) + min);
	};

	//Function to do the animations
	const animateSorting = (animations) => {
		setDisableButtons(true);
		const arrayBars = document.getElementsByClassName("arrayBar");
		for (let i = 0; i < animations.length; i++) {
			const isColorChange = i % 3 !== 1;
			if (isColorChange) {
				const [barOneIdx, barTwoIdx] = animations[i];
				const barOneStyle = arrayBars[barOneIdx].style;
				const barTwoStyle = arrayBars[barTwoIdx].style;
				const color = i % 3 === 0 ? "turquoise" : "#353535";
				setTimeout(() => {
					barOneStyle.backgroundColor = color;
					barTwoStyle.backgroundColor = color;
				}, i * (101 - animationSpeed));
			} else {
				setTimeout(() => {
					const [barOneIdx, newHeightOne, barTwoIdx, newHeightTwo] =
						animations[i];
					const barOneStyle = arrayBars[barOneIdx].style;
					barOneStyle.height = `${newHeightOne}px`;
					const barTwoStyle = arrayBars[barTwoIdx].style;
					barTwoStyle.height = `${newHeightTwo}px`;
				}, i * (101 - animationSpeed));
			}
		}

		setTimeout(() => {
			setDisableButtons(false);
		}, animations.length * (101 - animationSpeed));
	};
	const sortingMethod = () => {
		let animations = null;
		switch (sorting_method) {
			case "bubbleSort":
				animations = getBubbleSortAnimations(duplicateArray, arraySize);
				animateSorting(animations);
				break;
			case "selectionSort":
				animations = getSelectionSortAnimations(duplicateArray, arraySize);
				animateSorting(animations);
				break;
			case "quickSort":
				animations = getQuickSortAnimations(duplicateArray, arraySize);
				animateSorting(animations);
				break;
			case "heapSort":
				animations = getHeapSortAnimations(duplicateArray, arraySize);
				animateSorting(animations);
				break;
			case "insertionSort":
				const insertionSort = () => {
					setDisableButtons(true);
					// ref.current?.scrollIntoView({ behavior: "smooth" });
					const animations = getInsertionSortAnimations(
						duplicateArray,
						arraySize
					);
					const arrayBars = document.getElementsByClassName("arrayBar");
					for (let i = 0; i < animations.length; i++) {
						const isColorChange = i % 3 !== 1;
						if (isColorChange) {
							const [barOneIdx, barTwoIdx] = animations[i];
							const barOneStyle = arrayBars[barOneIdx].style;
							const barTwoStyle = arrayBars[barTwoIdx].style;
							const color = i % 3 === 0 ? "turquoise" : "##353535";
							setTimeout(() => {
								barOneStyle.backgroundColor = color;
								barTwoStyle.backgroundColor = color;
							}, i * (101 - animationSpeed));
						} else {
							setTimeout(() => {
								const [barIdx, newHeight] = animations[i];
								const barStyle = arrayBars[barIdx].style;
								barStyle.height = `${newHeight}px`;
							}, i * (101 - animationSpeed));
						}
					}
					setTimeout(() => {
						setDisableButtons(false);
					}, animations.length * (101 - animationSpeed));
				};
				break;
			default:
				break;
		}
	};

	const barWidth = arraySize > 50 ? 12 : arraySize > 25 ? 17 : 24;
	return (
		<div className="sorting">
			<div className="navbar">
				<div className="sliderContainer">
					<div className="size">
						<label htmlFor="slider">Size of Array: {arraySize}</label>
						<input
							type="range"
							id="slider"
							className="slider"
							min={5}
							max={100}
							value={arraySize}
							onChange={(e) => {
								setArraySize(e.target.value);
							}}
							disabled={disableButtons}
						/>
					</div>
					<div className="speed">
						<label htmlFor="Speedslider">Sorting Speed: {animationSpeed}</label>
						<input
							type="range"
							id="Speedslider"
							className="slider"
							min={1}
							max={100}
							value={animationSpeed}
							onChange={(e) => {
								setAnimationSpeed(e.target.value);
							}}
							disabled={disableButtons}
						/>
					</div>
				</div>

				<div className="buttons">
					<button
						className="ui button generate"
						disabled={disableButtons}
						onClick={resetArray}
					>
						Generate New Array
					</button>
					<button
						className="ui button"
						disabled={disableButtons}
						onClick={sortingMethod}
					>
						{sorting_text}
					</button>
				</div>
			</div>

			<div className="main" ref={ref}>
				{array.map((value, index) => {
					return (
						<div
							className="arrayBar"
							key={index}
							style={{
								height: `${value}px`,
							}}
						></div>
					);
				})}
			</div>
		</div>
	);
};

export default Visualiser;
