import React, { useState } from "react";
import "./CodeEditorStyles.css";

const CodeEditor = () => {
	const [code, setCode] = useState("");
	const [language, setLanguage] = useState("py");
	const [output, setOutput] = useState("");

	const handleSubmit = async (e) => {
		e.preventDefault(); // Prevent default form submission

		try {
			const response = await fetch("http://localhost:4000/run", {
				method: "POST",
				body: JSON.stringify({ language, code }),
				headers: { "Content-Type": "application/json" },
				credentials: "include",
			});

			if (!response.ok) {
				throw new Error(`HTTP error! status: ${response.status}`);
			}

			const responseData = await response.json();
			setOutput(responseData.output);
		} catch (error) {
			console.error("Error sending code:", error);
		}
	};
	return (
		<div className="code-editor-container">
			<h1>Code Editor</h1>
			<div className="functions">
				<select value={language} onChange={(e) => setLanguage(e.target.value)}>
					<option value="py">Python</option>
				</select>
				<button onClick={handleSubmit}>Submit</button>
			</div>
			<div className="input-output-container">
				<div className="input">
					<h2>Input</h2>
					<textarea
						value={code}
						onChange={(e) => {
							setCode(e.target.value);
						}}
					/>
				</div>
				<div className="output">
					<h2>Output</h2>
					<pre>{output}</pre>
				</div>
			</div>
		</div>
	);
};

export default CodeEditor;
