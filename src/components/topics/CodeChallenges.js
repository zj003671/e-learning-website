import React, { useState } from "react";
import CodeEditor from "./CodeEditor";

import "./CodeChallengesStyles.css";

const CodeChallenges = ({ CodeQuestions }) => {
	const [questionData, setQuestionData] = useState("");

	const handleQuestion = (value) => {
		const selectedQuestion = CodeQuestions.find(
			(question) => question.id === value
		);
		setQuestionData(selectedQuestion);
	};

	return (
		<div className="code-challenges-container">
			<h1>Questions</h1>
			<ul>
				{CodeQuestions.map((question) => (
					<li key={question.id} onClick={() => handleQuestion(question.id)}>
						{question.id}
					</li>
				))}
			</ul>
			{questionData && (
				<div className="code-questions">
					<h1>{questionData.title}</h1>
					<pre className="text">
						<span>
							Information
							<br />
						</span>
						{questionData.info}
					</pre>
					<pre className="text">
						<span>
							Task
							<br />
						</span>
						{questionData.task}
					</pre>
					<br />
					<pre className="code">{questionData.code}</pre>{" "}
					{/* Use <pre> for code formatting */}
					<br />
					<pre className="text">
						<span>
							Hint
							<br />
						</span>
						{questionData.hint}
					</pre>
				</div>
			)}
			<CodeEditor />
		</div>
	);
};

export default CodeChallenges;
