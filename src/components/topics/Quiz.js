import React, { useState } from "react";
import "./QuizStyles.css";
import { resultInitialState } from "./Questions";

const Quiz = ({ title, questions }) => {
	const [currentQuestion, setCurrentQuestion] = useState(0);
	const [answerIndex, setAnswerIndex] = useState(null);
	const [answer, setAnswer] = useState(null);
	const [result, setResult] = useState(resultInitialState);
	const [showResult, setShowResult] = useState(false);
	const [inputAnswer, setInputAnswer] = useState("");
	const { question, choices, correctAnswer, type } = questions[currentQuestion];

	const onAnswerClick = (answer, index) => {
		setAnswerIndex(index);
		if (answer === correctAnswer) {
			setAnswer(true);
		} else {
			setAnswer(false);
		}
	};

	const onClickNext = () => {
		setAnswerIndex(null);
		setResult((prev) =>
			answer
				? {
						...prev,
						score: prev.score + 5,
						correctAnswers: prev.correctAnswers + 1,
				  }
				: {
						...prev,
						wrongAnswers: prev.wrongAnswers + 1,
				  }
		);

		if (currentQuestion !== questions.length - 1) {
			setCurrentQuestion((prev) => prev + 1);
		} else {
			setCurrentQuestion(0);
			setShowResult(true);
		}
	};

	const handleInputChange = (evt) => {
		setInputAnswer(evt.target.value);
		if (evt.target.value === correctAnswer) {
			setAnswer(true);
		} else {
			setAnswer(false);
		}
	};

	const getAnswerUI = () => {
		if (type === "FIB") {
			return <input value={inputAnswer} onChange={handleInputChange} />;
		}
		return (
			<ul>
				{choices.map((choice, index) => (
					<li
						className={answerIndex === index ? "selected-answer" : null}
						key={choice}
						onClick={() => onAnswerClick(choice, index)}
					>
						{choice}
					</li>
				))}
			</ul>
		);
	};

	const onTryAgain = () => {
		setResult(resultInitialState);
		setShowResult(false);
	};

	return (
		<div className="quiz-container">
			<h1>{title}</h1>
			{!showResult ? (
				<>
					<p>
						<span className="active-question-no">{currentQuestion + 1}</span>
						<span className="total-question">/{questions.length}</span>
					</p>
					<h2>{question}</h2>
					{getAnswerUI()}
					<div className="quiz-footer">
						<button
							onClick={onClickNext}
							disabled={answerIndex === null && !inputAnswer}
						>
							{currentQuestion === questions.length - 1 ? "Finish" : "Next"}
						</button>
					</div>
				</>
			) : (
				<div className="result">
					<h3>Result</h3>
					<p>
						Score: <span>{result.score}</span>
					</p>
					<p>
						Total Questions: <span>{questions.length}</span>
					</p>
					<p>
						Correct Answers: <span>{result.correctAnswers}</span>
					</p>
					<p>
						Wrong Answers: <span>{result.wrongAnswers}</span>
					</p>
					<button onClick={onTryAgain}>Try Again</button>
				</div>
			)}
		</div>
	);
};

export default Quiz;
