import "./InformationStyles.css";

import React from "react";

const InformationData = ({ className, heading, text, img1, img2 }) => {
	return (
		<div className={className}>
			<div className="info-text">
				<h2>{heading}</h2>
				<p>{text}</p>
			</div>
			<div className="image">
				<img alt="img" src={img1} />
				<img alt="img" src={img2} />
			</div>
		</div>
	);
};

export default InformationData;
