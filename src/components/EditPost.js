import React, { useEffect, useState } from "react";
import "./CreatePostStyles.css";
import { Navigate, useParams } from "react-router-dom";
import Editor from "./Editor";

const EditPost = () => {
	const { id } = useParams();
	const [title, setTitle] = useState("");
	const [summary, setSummary] = useState("");
	const [content, setContent] = useState("");
	const [files, setFiles] = useState("");
	const [redirect, setRedirect] = useState(false);

	useEffect(() => {
		fetch("http://localhost:4000/post/" + id).then((response) => {
			response.json().then((postInfo) => {
				setTitle(postInfo.title);
				setContent(postInfo.content);
				setSummary(postInfo.summary);
			});
		});
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	async function updatePost(ev) {
		ev.preventDefault();
		const data = new FormData();
		data.set("title", title);
		data.set("summary", summary);
		data.set("content", content);
		data.set("id", id);
		if (files?.[0]) {
			data.set("file", files?.[0]);
		}
		const response = await fetch("http://localhost:4000/post", {
			method: "PUT",
			body: data,
			credentials: "include",
		});
		if (response.ok) {
			setRedirect(true);
		}
	}

	async function deletePost(ev) {
		ev.preventDefault();
		try {
			const response = await fetch("http://localhost:4000/post/" + id, {
				method: "DELETE",
				credentials: "include",
			});

			if (response.ok) {
				setRedirect(true); // Redirect on successful deletion
			} else {
				console.error("Failed to delete post:", response.statusText);
				// Handle deletion failure (e.g., display an error message to the user)
			}
		} catch (error) {
			console.error("Error deleting post:", error);
			// Handle network errors or other issues
		}
	}

	if (redirect) {
		return <Navigate to="/forum" replace />;
	}

	return (
		<>
			<h2>Edit Post?</h2>
			<form className="createPost">
				<input
					type="title"
					placeholder={"Title"}
					value={title}
					onChange={(ev) => setTitle(ev.target.value)}
				></input>
				<input
					type="summary"
					placeholder={"Summary"}
					value={summary}
					onChange={(ev) => setSummary(ev.target.value)}
				></input>
				<input type="file" onChange={(ev) => setFiles(ev.target.files)}></input>
				<Editor onChange={setContent} value={content} />
				<div className="button-group">
					<button className="delete-btn" type="button" onClick={deletePost}>
						Delete Post
					</button>
					<button type="submit" onClick={updatePost}>
						Update Post
					</button>
				</div>
			</form>
		</>
	);
};

export default EditPost;
