import { useNavigate } from "react-router-dom";
import "./TopicsStyles.css";

import React from "react";

const TripData = ({ image, heading, text, route }) => {
	const navigate = useNavigate();

	const navigateToTopic = () => {
		navigate(route);
	};

	return (
		<div className="t-card" onClick={navigateToTopic}>
			<div className="t-image">
				<img src={image} alt="img"></img>
			</div>
			<h4>{heading}</h4>
			<p>{text}</p>
		</div>
	);
};

export default TripData;
