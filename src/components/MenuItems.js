export const MenuItems = [
	{
		title: "Home",
		url: "/",
		cName: "nav-links",
		icon: "fa-solid fa-house-user",
	},
	{
		title: "About",
		url: "/about",
		cName: "nav-links",
		icon: "fa-solid fa-circle-info",
	},
	{
		title: "Forum",
		url: "/forum",
		cName: "nav-links",
		icon: "fa-solid fa-pen-to-square",
	},
	{
		title: "Topics",
		url: "/service",
		cName: "nav-links",
		icon: "fa-solid fa-briefcase",
	},
	{
		title: "Chatbot",
		url: "/chatbot",
		cName: "nav-links",
		icon: "fa-solid fa-robot",
	},
	{
		title: "Contact",
		url: "/contact",
		cName: "nav-links",
		icon: "fa-solid fa-address-book",
	},
];
