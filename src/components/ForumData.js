import React from "react";

import "./ForumDataStyles.css";
import { formatISO9075 } from "date-fns";
import { Link } from "react-router-dom";

const ForumData = ({
	_id,
	title,
	summary,
	cover,
	content,
	createdAt,
	author,
}) => {
	return (
		<>
			<Link to={`http://localhost:3000/post/${_id}`}>
				<div className="post">
					<img src={"http://localhost:4000/" + cover} alt="img"></img>
					<div className="texts">
						<h2>{title}</h2>
						<p className="info">
							<span href="/" className="author">
								{author.username}
							</span>
							<time>{formatISO9075(new Date(createdAt))}</time>
						</p>
						<p className="summary">{summary}</p>
					</div>
				</div>
			</Link>
		</>
	);
};

export default ForumData;
