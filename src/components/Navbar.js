import React from "react";
import { useContext, useState } from "react";
import { MenuItems } from "./MenuItems";
import "./NavbarStyles.css";
import { Link } from "react-router-dom";
import { UserContext } from "./UserContext";

export const Navbar = () => {
	const { userInfo, setUserInfo } = useContext(UserContext);

	function logout() {
		fetch("http://localhost:4000/logout", {
			credentials: "include",
			method: "POST",
		});
		setUserInfo(null);
	}

	const [clicked, setClicked] = useState(false);
	const handleClick = () => {
		setClicked(!clicked); // Update state using setClicked
	};

	const username = userInfo?.username;

	return (
		<nav className="NavbarItems">
			{!username && <h1 className="Navbar-logo">Educational</h1>}
			{username && (
				<h1 className="Navbar-logo">
					Educational{" "}
					<span className="username-title">, Hello {username}!</span>
				</h1>
			)}
			<div className="menu-icons" onClick={handleClick}>
				<i className={clicked ? "fas fa-times" : "fas fa-bars"}></i>
			</div>
			<ul className={clicked ? "Nav-menu active" : "Nav-menu"}>
				{MenuItems.map((item, index) => {
					return (
						<li key={index}>
							<Link className={item.cName} to={item.url}>
								<i className={item.icon}></i>
								{item.title}
							</Link>
						</li>
					);
				})}
				{!username && (
					<>
						<li>
							<Link className="nav-links" to="/login">
								<i className="fa-solid fa-right-to-bracket"></i>
								Login
							</Link>
						</li>
						<li>
							<Link className="nav-links" to="/signup">
								<i className="fa-solid fa-user-plus"></i>
								Signup
							</Link>
						</li>
					</>
				)}
				{username && (
					<>
						<li>
							<Link className="nav-links" to="/create">
								<i className="fa-solid fa-plus"></i>
								Create
							</Link>
						</li>
						<li>
							<Link className="nav-links" to="/" onClick={logout}>
								<i className="fa-solid fa-trash-can"></i>
								Logout ({username})
							</Link>
						</li>
					</>
				)}
			</ul>
		</nav>
	);
};
