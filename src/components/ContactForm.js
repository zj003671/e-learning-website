import "./ContactFormStyles.css";

import React from "react";

const ContactForm = () => {
	const handleSubmit = async (event) => {
		event.preventDefault();

		const name = event.target.name.value;
		const email = event.target.email.value;
		const subject = event.target.subject.value;
		const message = event.target.message.value;
	};

	return (
		<div className="form-container">
			<h1>Send a Message.</h1>
			<form action="/send_email" method="post">
				<input type="text" id="name" placeholder="Name"></input>
				<input type="email" id="email" placeholder="Email"></input>
				<input type="text" id="subject" placeholder="Subject"></input>
				<textarea id="message" placeholder="Message" rows="4"></textarea>
				<button type="submit" className="submit">
					Send Message
				</button>
			</form>
		</div>
	);
};

export default ContactForm;
