import React from "react";

import { Navbar } from "../components/Navbar";
import Hero from "../components/Hero";
import Footer from "../components/Footer";
import SignupForm from "../components/SignupForm";

const SignUp = () => {
	return (
		<>
			<Navbar />
			<Hero
				cName="hero-mid"
				heroImg="https://images.unsplash.com/photo-1529753253655-470be9a42781?q=80&w=1287&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
				title="SignUp"
				buttonClass="hide"
			/>
			<SignupForm />
			<Footer />
		</>
	);
};

export default SignUp;
