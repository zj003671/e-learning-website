import React from "react";

import { Navbar } from "../components/Navbar";
import Hero from "../components/Hero";
import Footer from "../components/Footer";
import EditPost from "../components/EditPost";

const Edit = () => {
	return (
		<>
			<Navbar />
			<Hero
				cName="hero-mid"
				heroImg="https://images.unsplash.com/photo-1618022325802-7e5e732d97a1?q=80&w=1348&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
				title="Edit"
				buttonClass="hide"
			/>
			<EditPost />
			<Footer />
		</>
	);
};

export default Edit;
