import React from "react";

import { Navbar } from "../components/Navbar";
import Hero from "../components/Hero";
import Footer from "../components/Footer";
import CreatePost from "../components/CreatePost";

const Create = () => {
	return (
		<div>
			<>
				<Navbar />
				<Hero
					cName="hero-mid"
					heroImg="https://images.unsplash.com/photo-1475257026007-0753d5429e10?q=80&w=1170&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
					title="Create"
					buttonClass="hide"
				/>
				<CreatePost />
				<Footer />
			</>
		</div>
	);
};

export default Create;
