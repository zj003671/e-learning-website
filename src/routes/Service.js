import React from "react";

import { Navbar } from "../components/Navbar";
import Hero from "../components/Hero";
import Footer from "../components/Footer";
import Topics from "../components/Topics";

const Service = () => {
	return (
		<>
			<Navbar />
			<Hero
				cName="hero-mid"
				heroImg="https://images.unsplash.com/photo-1544411047-c491e34a24e0?q=80&w=1170&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
				title="Topics"
				buttonClass="hide"
			/>
			<Topics />
			<Footer />
		</>
	);
};

export default Service;
