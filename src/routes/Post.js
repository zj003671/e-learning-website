import React, { useEffect, useState, useContext } from "react";

import { Navbar } from "../components/Navbar";
import Hero from "../components/Hero";
import Footer from "../components/Footer";
import { Link, useParams } from "react-router-dom";
import { UserContext } from "../components/UserContext";
import { formatISO9075 } from "date-fns";

import "../components/PostStyles.css";

const Post = () => {
	const [postInfo, setPostInfo] = useState(null);
	const { userInfo } = useContext(UserContext);
	const { id } = useParams();
	useEffect(() => {
		fetch(`http://localhost:4000/post/${id}`).then((response) => {
			response.json().then((postInfo) => {
				setPostInfo(postInfo);
			});
		});
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	if (!postInfo) return "";

	return (
		<>
			<Navbar />
			<Hero
				cName="hero-mid"
				heroImg={`http://localhost:4000/${postInfo.cover}`}
				title={postInfo.title}
				buttonClass="hide"
			/>
			<div className="post-page">
				<div className="info">
					<time>{formatISO9075(new Date(postInfo.createdAt))}</time>
					<p>
						by <span className="author">@{postInfo.author.username}</span>
					</p>
					{userInfo.id === postInfo.author._id && (
						<div className="edit-row">
							<Link className="edit-btn" to={`/edit/${postInfo._id}`}>
								<i className="fa-solid fa-pen-to-square"></i>
								Edit Post
							</Link>
						</div>
					)}
				</div>
				<div
					className="content"
					dangerouslySetInnerHTML={{ __html: postInfo.content }}
				/>
			</div>
			<Footer />
		</>
	);
};

export default Post;
