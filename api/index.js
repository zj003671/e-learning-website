const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const User = require("./models/User");
const Post = require("./models/Post");
const bcrypt = require("bcryptjs");
const app = express();
const jwt = require("jsonwebtoken");
const cookieParser = require("cookie-parser");
const multer = require("multer");
const uploadMiddleware = multer({ dest: "uploads/" });
const fs = require("fs");
const path = require("path");

const salt = bcrypt.genSaltSync(10);
const secret = "asdahsdvjhag3iu2g42983y";

app.use(cors({ credentials: true, origin: "http://localhost:3000" }));
app.use(express.json());
app.use(cookieParser());
app.use("/uploads", express.static(__dirname + "/uploads"));

mongoose.connect(
	"mongodb+srv://blog:nQUwUjSRBZjwGMLU@cluster0.mvl9dnj.mongodb.net/?retryWrites=true&w=majority"
);

app.post("/signup", async (req, res) => {
	const { username, password } = req.body;
	try {
		const userDoc = await User.create({
			username,
			password: bcrypt.hashSync(password, salt),
		});
		res.json(userDoc);
	} catch (e) {
		res.status(400).json(e);
	}
});

app.post("/login", async (req, res) => {
	const { username, password } = req.body;
	const userDoc = await User.findOne({ username });
	const passOk = bcrypt.compareSync(password, userDoc.password);
	if (passOk) {
		// logged in
		jwt.sign({ username, id: userDoc._id }, secret, {}, (err, token) => {
			if (err) throw err;
			res.cookie("token", token).json({
				id: userDoc._id,
				username,
			});
		});
	} else {
		res.status(400).json("wrong credentials");
	}
});

app.get("/profile", async (req, res) => {
	try {
		const token = req.cookies.token;

		// Handle missing token gracefully
		if (!token) {
			return res.status(401).json({ error: "Unauthorized" });
		}

		// Verify the JWT and extract user information
		const decoded = jwt.verify(token, secret);
		const { id } = decoded;

		// Fetch user data from the database (assuming a User model exists)
		const user = await User.findById(id);

		// Check if the user was found
		if (!user) {
			return res.status(404).json({ error: "User not found" });
		}

		// Send the user information as a response
		res.json({
			username: user.username,
			// Add other relevant user information you want to expose
		});
	} catch (err) {
		// Handle errors gracefully
		console.error("Error fetching profile:", err);
		res.status(500).json({ error: "Internal server error" });
	}
});

app.post("/logout", (req, res) => {
	res
		.cookie("token", "", { expires: new Date(0), httpOnly: true, secure: true })
		.json("ok");
});

app.post("/post", uploadMiddleware.single("file"), async (req, res) => {
	const { originalname, path } = req.file;
	const parts = originalname.split(".");
	const ext = parts[parts.length - 1];
	const newPath = path + "." + ext;
	fs.renameSync(path, newPath);

	const { token } = req.cookies;
	jwt.verify(token, secret, {}, async (err, info) => {
		if (err) throw err;

		const { title, summary, content } = req.body;
		const PostDoc = await Post.create({
			title,
			summary,
			content,
			cover: newPath,
			author: info.id,
		});
		res.json(PostDoc);
	});
});

app.put("/post", uploadMiddleware.single("file"), async (req, res) => {
	let newPath = null;
	if (req.file) {
		const { originalname, path } = req.file;
		const parts = originalname.split(".");
		const ext = parts[parts.length - 1];
		newPath = path + "." + ext;
		fs.renameSync(path, newPath);
	}

	const { token } = req.cookies;
	jwt.verify(token, secret, {}, async (err, info) => {
		if (err) throw err;
		const { id, title, summary, content } = req.body;
		const postDoc = await Post.findById(id);
		const isAuthor = JSON.stringify(postDoc.author) === JSON.stringify(info.id);
		if (!isAuthor) {
			return res.status(400).json("you are not the author");
		}
		await postDoc.updateOne({
			title,
			summary,
			content,
			cover: newPath ? newPath : postDoc.cover,
		});
		res.json(postDoc);
	});
});

app.get("/post", async (req, res) => {
	res.json(
		await Post.find()
			.populate("author", ["username"])
			.sort({ createdAt: -1 })
			.limit(20)
	);
});

app.get("/post/:id", async (req, res) => {
	const { id } = req.params;
	const postDoc = await Post.findById(id).populate("author", ["username"]);
	res.json(postDoc);
});

app.delete("/post/:id", async (req, res) => {
	try {
		const { id } = req.params;

		// Delete the post
		const postDoc = await Post.findById(id);
		const imagePath = postDoc.cover;
		// Delete the image file from the uploads directory
		if (imagePath) {
			fs.unlinkSync(path.join(__dirname, imagePath));
		}
		const deletedPost = await Post.findByIdAndDelete(id);

		if (!deletedPost) {
			return res.status(404).json({ message: "Post not found" });
		}

		// Successfully deleted, send response
		res.json({ message: "Post deleted successfully" });
	} catch (error) {
		console.error("Error deleting post:", error);
		res.status(500).json({ message: "Internal server error" });
	}
});

// CODE EDITOR

const { generateFile } = require("./code/generateFile");
const { executeFile } = require("./code/executeFile");

app.post("/run", async (req, res) => {
	const { language = "py", code } = req.body;

	if (code === undefined)
		return res.status(400).json({ success: false, error: "Empty code body." });
	try {
		const filepath = await generateFile(language, code);
		const output = await executeFile(filepath);
		return res.json({ filepath, output });
	} catch (err) {
		res.status(500);
	}
});

// CONTACT FORM

const nodemailer = require("nodemailer");

const emailService = "gmail";
const authUser = "ranjeth1965@gmail.com";
const authPass = "whfc mbol ikxe watj"; // Your email password

app.listen(4000);

// DATABASE PASSWORD: nQUwUjSRBZjwGMLU

// Connection String: mongodb+srv://blog:nQUwUjSRBZjwGMLU@cluster0.mvl9dnj.mongodb.net/?retryWrites=true&w=majority
