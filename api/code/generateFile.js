const fs = require("fs");
const path = require("path");

const dirCodes = path.join(__dirname, "input");

if (!fs.existsSync(dirCodes)) {
	fs.mkdirSync(dirCodes, { recursive: true });
}

const restrictedWords = ["import", "file"];

const generateFile = async (format, content) => {
	const jobId = "generatedFile";
	const filename = `${jobId}.${format}`;
	const filepath = path.join(dirCodes, filename);

	// Filter content
	const safeContent = content
		.split("\n")
		.filter((line) => !restrictedWords.some((word) => line.includes(word)))
		.join("\n");

	await fs.writeFileSync(filepath, safeContent);
	return filepath;
};

module.exports = {
	generateFile,
};
