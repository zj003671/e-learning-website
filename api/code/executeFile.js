const { exec } = require("child_process");

const fs = require("fs");
const path = require("path");

const outputPath = path.join(__dirname, "output");

if (!fs.existsSync(outputPath)) {
	fs.mkdirSync(outputPath, { recursive: true });
}

const executeFile = (filepath) => {
	const jobId = path.basename(filepath).split(".")[0];
	const outPath = path.join(outputPath, `${jobId}.out`);
	return new Promise((resolve, reject) => {
		exec(`python ${filepath} > ${outPath}`, (error, stdout, stderr) => {
			error && reject({ error, stderr });
			stderr && reject(stderr);
			resolve(fs.readFileSync(outPath, "utf-8"));
		});
	});
};

module.exports = {
	executeFile,
};
